package ru.yaleksandrova.tm;

import ru.yaleksandrova.tm.constant.ApplicationConst;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        System.out.println("** Welcome to Task Manager **");
        parseArgs(args);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            parseArg(command);
        }
    }

    public static void parseArg(final String arg) {
        if (ApplicationConst.ABOUT.equals(arg)) showAbout();
        if (ApplicationConst.VERSION.equals(arg)) showVersion();
        if (ApplicationConst.HELP.equals(arg)) showHelp();
        if (ApplicationConst.EXIT.equals(arg)) exitApplication();
    }

    public static void  parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
    }

    public static void exitApplication() {
        System.exit( 0 );
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Developer: Yulia Aleksandrova");
        System.out.println("E-mail: yaleksanrova@yandex.ru");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(ApplicationConst.ABOUT + " - Display developer info...");
        System.out.println(ApplicationConst.VERSION + " - Display program version...");
        System.out.println(ApplicationConst.HELP + " - Display list of commands...");
    }

}
